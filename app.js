const http = require('http');
const hostname = 'localhost';
const port = '3307';
const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hola Mundo!\n');
});